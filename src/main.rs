use log::{error, warn};
use std::collections::HashMap;
use std::fmt;
use std::fs;
use std::fs::DirEntry;
use std::path::Path;
#[derive(Debug)]
enum Error {
    IO(std::io::Error),
    Generic(String),
    OsNotSupported,
}

impl From<Error> for i32 {
    fn from(e: Error) -> Self {
        match e {
            Error::IO(_) => 1,
            Error::Generic(_) => 64,
            Error::OsNotSupported => 65,
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Self::IO(e)
    }
}

struct ConfigReader {
    map: HashMap<String, HashMap<String, String>>,
}

impl fmt::Display for ConfigReader {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (category, keyval) in &self.map {
            writeln!(f, "# {}", category)?;
            writeln!(f, "")?;
            writeln!(f, "Key | Value")?;
            writeln!(f, "---|-----")?;
            for (key, val) in keyval {
                writeln!(
                    f,
                    "{} | {}",
                    key.replace("\\", "."),
                    val.replace("\\", "\\\\")
                )?;
            }
        }
        writeln!(f, "")
    }
}

impl ConfigReader {
    fn from_ini<P: AsRef<Path>>(path: P) -> Result<Self, Error> {
        let mut config = Self {
            map: HashMap::new(),
        };

        //// This is fucking not OK for being Rust
        let filename = String::from(path.as_ref().file_name().unwrap().to_str().unwrap_or("?"));
        let buf = std::fs::read_to_string(&path)?;
        let mut entries = HashMap::new();
        let mut category = String::from("Unknown");
        for line in buf.lines() {
            let line = line.trim();
            if line.is_empty() || line.starts_with('#') {
                continue;
            }
            if line.starts_with('[') {
                if !line.contains(']') {
                    continue;
                }
                if let Some(c) = line.split(']').next() {
                    if !entries.is_empty() {
                        config.map.insert(category, entries);
                    }
                    category = c[1..].to_string().trim().into();
                    entries = HashMap::new();
                } else {
                    warn!(
                        "Ignored line: '{}': Missing ']' in file: {}",
                        line,
                        path.as_ref().display()
                    );
                }
            }
            if line.contains('=') {
                let keyval: Vec<&str> = line.split('=').collect();
                if keyval.len() != 2 {
                    warn!("Ignored: {}", line);
                    continue;
                }
                if keyval[1].is_empty() {
                    continue;
                }
                if keyval[1].contains("@ByteArray")
                    || keyval[1].contains("AAAA")
                    || keyval[1].starts_with("\\x")
                {
                    entries.insert(keyval[0].to_string(), String::from("Binary data"));
                } else {
                    entries.insert(keyval[0].to_string(), keyval[1].to_string());
                }
            }
        }
        if !entries.is_empty() {
            config.map.insert(category, entries);
        }

        Ok(config)
    }
}

fn run_main() -> Result<(), Error> {
    env_logger::init();
    let mut map = HashMap::new();
    let config_dir = dirs::config_dir().ok_or(Error::OsNotSupported)?;
    for entry in fs::read_dir(&config_dir)? {
        let entry = entry?;
        if entry.file_type().map_or(false, |ft| ft.is_file()) {
            let filename = String::from(entry.path().file_name().unwrap().to_str().unwrap_or("?"));
            match ConfigReader::from_ini(entry.path()) {
                Err(_) => {
                    error!("Could not read '{}' as an INI", entry.path().display());
                }
                Ok(config) => {
                    map.insert(filename, config);
                }
            }
        } else {
            warn!("Ignored directory: '{}'", entry.path().display());
        }
    }

    for (app, config) in map {
        println!("# {}", app);
        println!("#{}", config);
        println!("\\pagebreak");
    }

    Ok(())
}

fn main() {
    if let Err(e) = run_main() {
        eprintln!("{:?}", e);
        std::process::exit(i32::from(e));
    }
}
